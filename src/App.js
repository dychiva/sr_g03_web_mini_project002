import React from 'react';
import './App.css';
import { useTranslation } from 'react-i18next';
import './i18next';
import ArticleList from './components/ArticleList/ArticleList'
import Categories from './components/Categories/Categories'
import Header from './components/Header/Header'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
// import "bootstrap/dist/css/bootstrap.min.css";
import 'bootswatch/dist/lumen/bootstrap.min.css';
import ViewArticle from './components/ViewArticle/ViewArticle';
import AddArticle from './components/AddArticle/AddArticle';


function App() {
  const { t , i18n} = useTranslation();
  
  function handleClick(lang) {
    i18n.changeLanguage(lang);
  }

  return (
    <div className="App">
    
        <Router>
          <Header forTranslateEn={(props)=>handleClick('en')} forTranslateKh={(props)=>handleClick('kh')} translate={t}></Header>
          <Switch>
            <Route exact path="/"  render={(props)=><ArticleList translate={t} ></ArticleList>}/>
            <Route path="/Category"  render={(props)=><Categories translate={t} ></Categories>}/>
            <Route path="/Add" component={AddArticle}/>
            <Route path="/View/:id" component={ViewArticle}/>
          </Switch>

        </Router>
      
    </div>
  );
}

export default App;
