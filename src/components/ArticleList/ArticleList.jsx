import React, { Component } from "react";
import { Container, Row, Col,Form, Button,span } from "react-bootstrap";
import {fetchArticles, deleteArticle,getCategory, viewArticle,searchArticle} from '../../redux/actions/article/articleAction';
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'
import {Link} from 'react-router-dom';

class ArticleList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.fetchArticles();
    this.props.getCategory();
  }

  //Convert Date
  convertDate = (created_date) => {
    let parameterDate = created_date;
    let year = parameterDate.substring(0, 4);
    let month = parameterDate.substring(5, 7);
    let day = parameterDate.substring(8, 10);
    let formalDate = year + "-" + month + "-" + day;
    return formalDate;
  };
  handleSearch=(event)=>{
    this.props.searchArticle(event.target.value)
    console.log(event.target.value)
}
filterCategory=(event)=>{
  this.props.getCategory(event.target.value)
  console.log(event.target.value)
}


  render() {
    console.log(this.props.category)
    return (
      <div>
        <Container fluid="sm">
          <Row className="mt-3">
            <Col xs={5}>
              <h1>{this.props.translate("ArticleList.MANAGEMENT")}</h1>
            </Col>
            <Col>
              <Row>
                <Col>
                  <Form inline>
                    <Form.Label
                      className="my-1 mr-2"
                      htmlFor="inlineFormCustomSelectPref"
                    >
                      Category
                    </Form.Label>

                  <div className="mr-1">
                    <select className="browser-default custom-select">
                    <option>All</option>
                    {this.props.category.map((option)=>{
                        return <option onChange={this.filterCategory.bind(ArticleList)} key={option.id} value={option.name}>{option.name}</option>
                    })}

                    </select>
                </div>

                      <Form inline>
                      {/* <Form.Control type="text" placeholder="Search" className="mr-sm-1"  onChange={this.handleSearch.bind(ArticleList)} value={this.state.title}/> */}
                      <Form.Control type="text" placeholder="Filter by Title" className="mr-sm-1"  onChange={this.handleSearch.bind(ArticleList)} />
                      <Button variant="outline-success" className="mx-1"> {this.props.translate("ArticleList.SEARCH")} </Button>
                      <Link to="/Add">
                      <Button type="submit" className="ml-2">
                     {this.props.translate("ArticleList.ADD_ARTICLE")}
                     </Button>
                    </Link>
                      </Form>
                

                  </Form>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
        <br/>
        {
          this.props.article.map((data,index)=>{
            return <div key={index}>
                <Container>
                  <Row>
                    <Col xs={4}> <img src={data.image} width="100%" alt={data.image}/></Col>
                    <Col xs={8}>
                        <h5>{data.title}</h5>
                        <p className="text-muted">{this.props.translate("ArticleList.CREATE_DATE")} : {this.convertDate(data.createdAt)}</p>
                        <p className="text-muted">{data.category == null ? "No Type" : data.category.name}</p>
                        <p>{data.description}</p>
                        <Row>
                          <Col>

                          {/* <Link to="/View">
                          <Button variant="primary" onClick={()=>{this.props.viewArticle(data._id)}}>VIEW</Button>	&nbsp;
                          </Link> */}

                          <Link to={`/View/${data._id}`}>
                          <Button variant="primary"> {this.props.translate("ArticleList.VIEW")} </Button>	
                          </Link>
                          
                           {/* <Button variant="warning" onClick={()=>{this.props.updateArticle(data)}}>EDIT</Button>	&nbsp; */}
                           <Button variant="warning" className="mx-2"> {this.props.translate("ArticleList.EDIT")} </Button>
                           <Button variant="danger"  onClick={()=>{this.props.deleteArticle(data._id) }}> {this.props.translate("ArticleList.DELETE")} </Button>
                          </Col>
                        </Row>
                    </Col>
                  </Row>
                  <hr />
                </Container>
                <br/>
                
               
            </div>
          // );
        })}

        <p className="text-center">
          Yay! You have seen it all <span role="img">👍🏌️‍♂️🥃</span>
        </p>
      </div>
    );
  }
}



const mapStateToProps = (state) =>{
  return{
    article : state.articleReducer.article,
    articleDetail : state.articleReducer.articleDetail,
    category:state.articleReducer.categories
  }
}

const mapDispatchToProps = (dispatch)=>{
  const boundActionCreators = bindActionCreators({
    fetchArticles,
    deleteArticle,
    viewArticle,
    getCategory,
    searchArticle
  },dispatch)
  return boundActionCreators
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleList);
