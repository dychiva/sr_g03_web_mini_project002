import {Row,Col,Card} from 'react-bootstrap';
import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {viewArticle} from '../../redux/actions/article/articleAction';
import {connect} from 'react-redux'
import { bindActionCreators } from 'redux'

class ArticleList extends Component {
  constructor(props){
    super(props);
    this.state={
    }
  }
  componentWillMount() {
        var id = this.props.match.params.id;
        this.props.viewArticle(id);
  }

    //Convert Date
    convertDate = (created_date) =>{
      let parameterDate = created_date;
      let year = parameterDate.substring(0,4);
      let month = parameterDate.substring(5,7);
      let day = parameterDate.substring(8,10);
      let formalDate = year+ "-" + month + "-" + day;
      return formalDate;
  }

  render() {
      
      
    return (
  <div className="container mt-3">
    <Row >
        <Col lg={4} md={4}>
            <Card>
                <Card.Img variant="top" src={this.props.articleDetail.image} width="100%" alt={this.props.articleDetail.image} />
            </Card>
        </Col>
        <Col lg={8} md={8}>
            <div>
                <h3>{this.props.articleDetail.title}</h3>
                <p>Create Date:<span>2012-12-06</span></p>
                <p>Category-SPORN</p>
                <div>
                    <p>{this.props.articleDetail.description}</p>
                </div>
            </div>
        </Col>
    </Row>
  </div>
    );
  }
}


const mapStateToProps = (state) =>{
  return{
    articleDetail : state.articleReducer.articleDetail,
  }
}

const mapDispatchToProps = (dispatch)=>{
  const boundActionCreators = bindActionCreators({
    viewArticle
  },dispatch)
  return boundActionCreators
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleList)