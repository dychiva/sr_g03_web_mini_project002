import React, { Component } from "react";
import { Form, Button, Row, Image, Col } from "react-bootstrap";
import { addArticle } from "../../redux/actions/article/articleAction";
import { addImg } from "../../redux/actions/file/fileAction";
import { fetchCategories } from "../../redux/actions/category/categoryAction";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Axios from "axios";

class AddArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      category: "",
      imageFile: "",
      isSubmit: false,
      img: null,
    };
  }

  componentDidMount() {
    this.props.fetchCategories();
  }

  customFileInput = (event) => {
    const reader = new FileReader();
    let file = event.target.files[0];
    reader.onloadend = () => {
      if (reader.readyState === 2) {
        this.setState(
          {
            imageFile: reader.result,
            img: file,
          },
          () => console.log(this.state.img)
        );
      }
    };
    reader.readAsDataURL(file);
  };
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    let article = {};
    const formData = new FormData();

    formData.append("image", this.state.img, this.state.img.name);

    Axios.post("http://110.74.194.125:3535/api/images", formData, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
    }).then((res) => {
      article.title = this.state.title;
      article.description = this.state.description;
      article.category = this.state.category;
      article.image = res.data.url;
      this.props.addArticle(article);
    });
    console.log(article);
  };
  render() {
    const { imageFile } = this.state;
    return (
      <div className="container">
        <Row>
          <Col>
            <h1 className="mt-4">Add Article</h1>
            <div className="row-left">
              <form onSubmit={this.handleSubmit}>
                <Form>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Title</Form.Label>
                    <Form.Control
                      id="title"
                      name="title"
                      type="text"
                      value={this.state.title}
                      placeholder="Input title"
                      aria-describedby="inputGroupPrepend"
                      required
                      onChange={this.handleChange}
                    />
                  </Form.Group>
                  <fieldset>
                    <Form.Group as={Row}>
                      <Form.Label as="legend" column sm={2}>
                        Category :
                      </Form.Label>

                      {this.props.category.map((type, index) => {
                        return (
                          <Form.Check
                            key={index}
                            style={{ float: "left", marginLeft: "10px" }}
                            type="radio"
                            label={type.name}
                            className="mt-2"
                            name="category"
                            id="category"
                            value={type._id}
                            onChange={this.handleChange}
                          />
                        );
                      })}
                    </Form.Group>
                  </fieldset>
                  <Form.Group controlId="formBasicPassword">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                      name="description"
                      id="description"
                      type="text"
                      placeholder="Input Descrition"
                      value={this.state.description}
                      aria-describedby="inputGroupPrepend"
                      required
                      onChange={this.handleChange}
                    />
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Thumbnail</Form.Label>
                    <Form.File
                      id="custom-file"
                      label={
                        this.state.img === null
                          ? "Add image"
                          : this.state.img.name
                      }
                      custom
                      onChange={this.customFileInput}
                    />
                  </Form.Group>
                  <Button variant="primary" type="submit">
                    SUBMIT
                  </Button>
                </Form>
              </form>
            </div>
          </Col>
          <Col xs={6} md={4}>
            <div className="row-right">
              <Image
                style={{
                  width: 400,
                  height: 270,
                  overflow: "hidden",
                  marginTop: 115,
                }}
                src={
                  imageFile === ""
                    ? " https://www.eltis.org/sites/default/files/default_images/photo_default_4.png"
                    : imageFile
                }
                alt="Default"
              />
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    article: state.articleReducer.article,
    category: state.categoryReducer.category,
    url: state.fileReducer.url,
  };
};

const mapDispatchToProps = (dispatch) => {
  const boundActionCreators = bindActionCreators(
    {
      addArticle,
      addImg,
      fetchCategories,
    },
    dispatch
  );
  return boundActionCreators;
};

export default connect(mapStateToProps, mapDispatchToProps)(AddArticle);
