import React, { Component } from "react";
import { Form, Button, Table } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  fetchCategories,
  addCategory,
  deleteCategory,
  editCategory,
} from "../../redux/actions/category/categoryAction";

class Categories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category: { name: "" },
      isEdit: false,
      id: "",
    };
  }

  componentDidMount() {
    this.props.fetchCategories();
  }

  handleChange = (event) => {
    this.setState({
      category: {
        ...this.state.category,
        name: event.target.value,
      },
    });
  };

  handleEdit = (id, name) => {
    this.setState({
      category: {
        ...this.state.category,
        name: name,
      },
      id: id,
      isEdit: true,
    });
  };

  handleAdd = () => {
    if (this.state.isEdit)
      this.props.editCategory(this.state.id, this.state.category);
    else this.props.addCategory(this.state.category);
  };

  render() {
    return (
      <div className="container mt-3">
        <h1>{this.props.translate("Category.CATEGORY")}</h1>
        <Form>
          <Form.Label>{this.props.translate("Category.NAME")}</Form.Label>
          <Form.Group className="form-inline">
            <Form.Control
              type="text"
              value={this.state.category.name}
              placeholder="Input Category name"
              className="mr-2"
              onChange={this.handleChange}
            />
            <Button variant="secondary" type="button" onClick={this.handleAdd}>
              {this.state.isEdit
                ? "Save"
                : this.props.translate("Category.ADD_CATEGORY")}
            </Button>
          </Form.Group>
        </Form>

        <Table striped bordered hover>
          <thead className="text-center">
            <tr>
              <th>#</th>
              <th>{this.props.translate("Category.NAME")}</th>
              <th>{this.props.translate("Category.ACTION")}</th>
            </tr>
          </thead>
          <tbody className="text-center">
            {this.props.category.map((data, index) => {
              return (
                <tr key={data._id}>
                  <td>{index + 1}</td>
                  <td>{data.name}</td>
                  <td>
                    <Button
                      variant="info"
                      type="button"
                      className="mr-2"
                      onClick={() => this.handleEdit(data._id, data.name)}
                    >
                      {this.props.translate("Category.EDIT")}
                      {/* <Button variant="info" type="button" className="mr-2">
                      {this.props.translate("Category.ADD_CATEGORY")} */}
                    </Button>
                    <Button
                      variant="danger"
                      type="button"
                      onClick={() => {
                        this.props.deleteCategory(data._id);
                      }}
                    >
                      {this.props.translate("Category.DELETE")}
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    category: state.categoryReducer.category,
  };
};

const mapDispatchToProps = (dispatch) => {
  const boundActionCreators = bindActionCreators(
    {
      fetchCategories,
      addCategory,
      deleteCategory,
      editCategory,
    },
    dispatch
  );
  return boundActionCreators;
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
