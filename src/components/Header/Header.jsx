import React, { Component } from "react";
import { Nav, Container,Navbar, Form, FormControl, Button , NavDropdown} from "react-bootstrap";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

export default class Header extends Component {
  render() {
    return (
      <div>
        <Navbar className=" navbar navbar-expand-lg navbar-light bg-light">
         <div className="container">
        
           
          <Navbar.Brand as={Link} to="/">
          <img as={Link} to="/Category"
              width={50}
              src="https://z-p3-scontent.fpnh5-1.fna.fbcdn.net/v/t1.0-9/81386493_2900555833322218_3950515716339269632_n.png?_nc_cat=110&_nc_sid=85a577&_nc_eui2=AeFFy9Ue_ryOCAedyi2BJ6fplIJUNTNkCtWUglQ1M2QK1R2kdbEyZkP_AVKAL4CVcnFeARsRAOEnfKuQCHUTKb91&_nc_ohc=-s40XZPTcSIAX98aKtN&_nc_ht=z-p3-scontent.fpnh5-1.fna&oh=e0fac6043f3dd1ca49348dd9760205c4&oe=5F172C29"
              alt="KSHRD"
              className="mx-3"
            />
           {this.props.translate("Header.ARTICLE_MANAGEMENT")}
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link as={Link} to="/" >
                {this.props.translate("Header.ARTICLE")} 
              </Nav.Link>
              <Nav.Link as={Link} to="/Category">
              {this.props.translate("Header.CATEGORY")} 
              </Nav.Link>
              <NavDropdown  title={this.props.translate("Header.LANGUAGE")} id="collasible-nav-dropdown">
              <NavDropdown.Item  onClick={()=>this.props.forTranslateKh('kh')}>{this.props.translate("Header.KHMER")}  </NavDropdown.Item>
                <NavDropdown.Item  onClick={()=>this.props.forTranslateEn('en')}>{this.props.translate("Header.ENGLISH")}  </NavDropdown.Item>
      </NavDropdown>
            </Nav>
          </Navbar.Collapse>       
          </div>
        </Navbar>
      </div>
    );
  }
}
