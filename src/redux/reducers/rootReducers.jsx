import { articleReducer } from "./article/articleReducer";
import { combineReducers } from "redux";
import { categoryReducer } from "./category/categoryReducer";
import { fileReducer } from "./file/fileReducer";
const reducer = {
  articleReducer: articleReducer,
  categoryReducer: categoryReducer,
  fileReducer: fileReducer,
};

export const rootReducer = combineReducers(reducer);
