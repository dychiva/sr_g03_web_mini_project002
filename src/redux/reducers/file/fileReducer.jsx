import { ADD_IMG } from "../../actions/file/actionType";

const initState = {
  url: "",
};

export const fileReducer = (state = initState, action) => {
  switch (action.type) {
    case ADD_IMG:
      return {
        ...state,
        url: action.url,
      };
    default:
      return state;
  }
};
