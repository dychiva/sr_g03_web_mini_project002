import Axios from "axios";
import {FETCH_ATICLES,GET_CATEGORY, DELETE_ARTICLE, VIEWARTICLE, EDIT_ARTICLE,ADD_ARTICLE,SEARCH_ARTICLE} from './actionType';

export const fetchArticles = () =>{
    const innerFetchArticles = async (dispatch) =>{
        const result = await Axios.get("http://110.74.194.125:3535/api/articles")
        dispatch({
            type : FETCH_ATICLES,
            article : result.data.data
        })
    }
    return innerFetchArticles;
    
}

export const deleteArticle = (id) =>{
    debugger;
    const innerDeletetArticle = async (dispatch) =>{
        const result = await Axios.delete("http://110.74.194.125:3535/api/articles/"+id)
        dispatch({
            type : DELETE_ARTICLE,
            id:id
        });
    }
    return innerDeletetArticle;

}


export const viewArticle = (id) =>{
    debugger;
    const innerDeletetArticle = async (dispatch) =>{
        const result = await Axios.get("http://110.74.194.125:3535/api/articles/"+id)
        dispatch({
            type : VIEWARTICLE,
            article : result.data.data
        });
    }
    return innerDeletetArticle;

}
export const addArticle = (article)=>{
    
     debugger;
    const innerAddArticle = async (dispatch) =>{
        const result = await Axios.post("http://110.74.194.125:3535/api/articles",article)
        dispatch({
            type : ADD_ARTICLE,
            article : result.data.data
        });
        
    }
    return innerAddArticle
}
export const searchArticle = (title) => {
    const innerSearch=async(dispatch)=>{
        const result=await Axios.get(`http://110.74.194.125:3535/api/articles?title=${title}`)
        dispatch({
            type:SEARCH_ARTICLE,
            article: result.data.data
        })
    }
    return innerSearch
};
export const getCategory = () =>{
    const innerGetCategory = async (dispatch) =>{
        const category = await Axios.get("http://110.74.194.125:3535/api/category")
        dispatch({
            type:GET_CATEGORY,
            article: category.data.data
        })
    }
    return innerGetCategory
}
// export const updateArticle = (paramObje) =>{
//     // paramObje  get from form
//     // paramObje={
//     //     "title": "string",
//     //     "description": "string",
//     //     "published": true,
//     //     "image": "string"
//     // }
//     debugger;
//     const innerDeletetArticle = async (dispatch) =>{
//         // const result = await Axios.putch("http://110.74.194.125:3535/api/articles/"+paramObje.id)
//         dispatch({
//             type : EDIT_ARTICLE,
//             paramObje : paramObje
//         });
//     }
//     return innerDeletetArticle;

// }
