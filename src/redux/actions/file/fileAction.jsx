import Axios from "axios";
import { ADD_IMG } from "./actionType";
export const addImg = (img) => {
  const innerAddImg = async (dispatch) => {
    const result = await Axios.post(
      "http://110.74.194.125:3535/api/images",
      img,
      {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
      }
    );
    dispatch({
      type: ADD_IMG,
      url: result.data.url,
    });
  };
  return innerAddImg;
};
